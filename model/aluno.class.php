
<?php 

class Aluno {

    private    $intIdAluno;
    private    $vchMatricula;
    private    $vchNome;
    private    $intIdade;
    private    $bolSexo;       /*0 masculino - 1 feminino */
    private    $vchCpf;
    private    $vchTelefone;
    private    $vchEmail;
    private    $tnyEstCivil;
    

    public function __contruct(){}
    

    public function setIntIdAluno($pIdAluno){
        $this->intIdAluno = $pIdAluno;
    }
    
    public function getIntIdAluno(){
        return $this->intIdAluno;
    }

    public function setVchMatricula($pMatricula){
        $this->vchMatricula = $pMatricula;
    }

    public function getVchMatricula(){
        return $this->vchMatricula;
    }

    public function setVchNome($pNome){
        $this->vchNome = $pNome;
    }
    
    public function getVchNome(){
        return $this->vchNome;
    }
    public function setIntIdade($pIdade){
        $this->intIdade = $pIdade;
    }

    public function getIntIdade(){
        return $this->intIdade;
    }

    public function setBolSexo($pSexo){
        $this->bolSexo = $pSexo;
    }

    public function getBolSexo(){
        return $this->bolSexo;
    }
    
    public function setVchCpf($pCpf){
        $this-> vchCpf = $pCpf;
    }

    public function getVchCpf(){
        return $this->vchCpf;
    }
        
    public function setVchTelefone($pTelefone){
        $this-> vchTelefone = $pTelefone;
    }

    public function getVchTelefone(){
        return $this->vchTelefone;
    }

    public function setVchEmail($pEmail){
        $this->vchEmail = $pEmail;
    }

    public function getVchEmail(){
        return $this->vchEmail;
    }

    public function setTnyEstCivil($pEstCivil){
        $this->tnyEstCivil = $pEstCivil;
    }

    public function getTnyEstCivil(){
        return $this->tnyEstCivil;
    }



    
}

?>