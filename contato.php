<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <link rel="icon" href="./img/icon.png">
        <title>Contato</title>
        
        <link rel="stylesheet" href="./css/bootstrap.css">
        <link rel="stylesheet" href="./css/bootstrap-theme.css">
        <link rel="stylesheet" href="./css/personalizacao.css">
        
        <script src="./js/jQuery/jquery-2.1.4.js"></script>
        <script src="js/bootstrap.js"></script>
    </head>
    <body>
        <div class="container conteudo-principal">
            <header>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="page-header">
                            <h1>Contato <small>Fale conosco</small></h1>
                        </div>    
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <nav role="navigation" class="navbar navbar-default">
                            <div class="navbar-header">
                                <a class="navbar-brand" href="index.php">
                                    <img alt="Brand" src="./img/icon.png">
                                </a>
                                <button  type="button" data-target="#navBar" data-toggle="collapse" class="navbar-toggle">
                                    <span class="sr-only">Menu alternativo</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div id="navBar" class="collapse navbar-collapse">
                                <ul class="nav navbar-nav navbar-static-top">
                                    <li><a href="index.php">Home</a></li>
                                    <!-- dropdown menu alunos -->
                                    <li role="presentation" class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" > Alunos <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="./view/alunos/add.php">Cadastrar Aluno</a></li>
                                                <li><a href="./view/alunos/view.php">Visualizar Matriculados</a></li>
                                                <li class="disabled"><a href="#">Atualizar dados do aluno</a></li>
                                                <li class="disabled"><a href="#">Apagar Aluno</a></li>
                                            </ul>
                                    </li>
                                    <li class="active"><a href="contato.php">Fale Conosco</a></li>
                                </ul>
                            </div>
                            
                        </nav>
                    </div>
                </div>
            </header>
            
            <section>
                
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        
                        <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h2 class="text-center">Sua opnião é importante para nós.</h2>
                              </div>
                              <div class="panel-body">
                                
                                  <p class="lead text-center">Nós prezamos sempre pelo bom tratamento e bem estar dos nossos alunos e visitantes, por isso estamos sempre dispostos a melhorar
                        nossa qualidade de trabalho oferecendo sempre a melhor forma de atendimento, garantindo assim mais conforto a todos.</p>
                        
                                  <p class="lead text-center" >Gostariamos de saber a sua opnião a respeito da nossa plataforma online: criticas, sugestões, elogios, todos serão bem vindos. Preencha os
                        campos abaixo e envie-nos a sua opnião.</p>
                                  
                                  
                                  
                                  
                         <form class="form-horizontal">                  <!-- definir parametros do formulário-->
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12 col-md-1">
                                    <label class="control-label">Nome</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <input class="form-control " type="text" name="vchNomeCompleto" placeholder="Nome Completo" required>
                                </div>    
                            </div>
                            
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12 col-md-1">
                                    <label class="control-label ">Email</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <input class="form-control form-control " type="email"  name="vchEmail" placeholder="Email" required>
                                </div>
                                
                            </div>
                            
                            <div class="form-group">
                                 <div class=" control-label col-xs-12 col-sm-12 col-md-1">
                                    <label class="control-label">Assunto</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <input class="form-control form-control " type="text"  name="vchAssunto" placeholder="Assunto" required>
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12 col-md-1">
                                    <label class="control-label ">Mensagem</label>
                                </div>
                                <div class="col-xs-12-col-sm-12 col-md-11">
                                    <textarea class="form-contol form-control col-xs-12 col-sm-12 col-md-11" rows="5" placeholder="Escreva aqui sua mensagem" required></textarea>
                                </div>    
                            </div>
                            
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-12 col-md-1"></div>
                                <div class="col-xs-12 col-sm-12 col-md-10">
                                    <button class="btn btn-success" type="submit">Enviar</button>
                                    <button class="btn btn-danger" type="reset">Apagar campos</button>
                                </div>
                            </div>
                            
                            
                        </form>
                              </div>
                            
                            
                        </div> <!--Fim do painel -->
                        
                        
                        
                       
                    </div>
                </div>
            </section>
            
            <footer>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="well well-sm">
                            <p class="text-center">&copy; Copyright 2015 Jeferson Oliveira <br>
                               &nbsp; &nbsp; &nbsp; &nbsp; <a href="http://www.facebook.com/profile.php?id=100005197385523" target="_blank">Facebook</a> | <a href="https://twitter.com/Jeffoliver97" target="_blank">Twiter</a>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </body>
</html>