<?php
    
    require './kint/Kint.class.php';
    require '../model/aluno.class.php';


    
    $novoAluno = new Aluno();

    if(isset($_POST["vchNome"]) && $_POST['vchNome'] !=''){
        $novoAluno->setVchNome($_POST["vchNome"]);
    }else{
        header("location :../view/alunos/add.php?strMsg=O campo 'Nome' não pode ser vazio&tipoMsg=erro");
    }

    if(isset($_POST["intIdade"]) && $_POST['intIdade'] !=''){
        $novoAluno->setIntIdade($_POST["intIdade"]);
    }else{
        header("location :../view/alunos/add.php");
    }

    if(isset($_POST["bolSexo"]) && $_POST['bolSexo'] !=''){
           if($_POST["bolSexo"] == "M"){
               $novoAluno->setBolSexo(0);
           }else{
               $novoAluno->setBolSexo(1);
           }        
    }else{
        header("location :../view/alunos/add.php");
    }

    if(isset($_POST["vchCpf"]) && $_POST['vchCpf'] !=''){
        $novoAluno->setVchCpf($_POST["vchCpf"]);
    }else{
        header("location :../view/alunos/add.php");
    }

    if(isset($_POST["vchTelefone"]) && $_POST['vchTelefone'] !=''){
        $novoAluno->setVchTelefone($_POST["vchTelefone"]);
    }else{
        header("location :../view/alunos/add.php");
    }

    if(isset($_POST["vchEmail"])){
        $novoAluno->setVchEmail($_POST["vchEmail"]);
    }else{
        header("location :../view/alunos/add.php");
    }

    if(isset($_POST["tnyEstCivil"]) && $_POST['tnyEstCivil'] !=''){
        if($_POST["tnyEstCivil"]=="ST"){
            $novoAluno->setTnyEstCivil(1);
        }else{
            if($_POST["tnyEstCivil"]=="CS"){
                $novoAluno->setTnyEstCivil(2);
             }else{
                 if($_POST["tnyEstCivil"] == "DV"){
                     $novoAluno->setTnyEstCivil(3);
                 }else{
                     $novoAluno->setTnyEstCivil(4);
                 }
             }
        }
        /*   Valores:
            1 - Solteiro
            2 - Casado
            3 - Divorciado
            4 - Viúvo
         */
    }else{
        header("location :../view/alunos/add.php");
    }






    $con = new PDO("mysql:host=127.0.0.1;dbname=escola","root","");

    $stmt = $con->prepare('insert into alunos (vchNome,intIdade,bolSexo
    ,vchCpf,vchTelefone,vchEmail,tnyEstCivil) values (?,?,?,?,?,?,?)');
    $stmt->bindValue(1,$novoAluno->getVchNome());
    $stmt->bindValue(2,$novoAluno->getIntIdade());
    $stmt->bindValue(3,$novoAluno->getBolSexo());
    $stmt->bindValue(4,$novoAluno->getVchCpf());
    $stmt->bindValue(5,$novoAluno->getVchTelefone());
    $stmt->bindValue(6,$novoAluno->getVchEmail());
    $stmt->bindValue(7,$novoAluno->getTnyEstCivil());
                                   
    $stmt->execute();

    if($stmt->errorCode()>0){
        header("location: .././view./alunos/add.php?strMsg=Erro de Cadastro&tipoMsg=erro");
    }else{
        header("location: .././view./alunos/add.php?strMsg=Aluno matriculado com sucesso! &tipoMsg=sucesso");
    }

    $ultimoId = $con->lastInsertId();
    $novoAluno->setVchMatricula('2015'.$ultimoId);
    $stmtUpdate = $con->prepare("update alunos set vchMatricula=? where intIdAluno=? ");
    $stmtUpdate->bindValue(1,$novoAluno->getVchMatricula());
    $stmtUpdate->bindValue(2,$ultimoId);
    $stmtUpdate->execute();
        
  
    if($stmtUpdate->errorCode()>0){
        d($stmtUpdate->erroInfo());
    }


    

?>