<?php
    
    require './kint/Kint.class.php';
    require '../model/aluno.class.php';

    
   
    $alunoEdit = new Aluno();
    $idAlunoEdit = $_POST['idAluno'];  // pego a id do aluno passado pela tela de edição

    if(isset($_POST['idAluno'])){  // se a variável 'idAluno' foi passada via get 
        
        
    if(isset($_POST["vchNome"]) && $_POST['vchNome'] !=''){
        $alunoEdit->setVchNome($_POST["vchNome"]);
    }else{
        header("location :../view/alunos/edit.php?strMsg=O campo 'Nome' não pode ser vazio&tipoMsg=erro");
    }

    if(isset($_POST["intIdade"]) && $_POST['intIdade'] !=''){
        $alunoEdit->setIntIdade($_POST["intIdade"]);
    }else{
        header("location :../view/alunos/edit.php");
    }

    if(isset($_POST["bolSexo"]) && $_POST['bolSexo'] !=''){
           if($_POST["bolSexo"] == "M"){
               $alunoEdit->setBolSexo(0);
           }else{
               $alunoEdit->setBolSexo(1);
           }        
    }else{
        header("location :../view/alunos/edit.php");
    }

    if(isset($_POST["vchCpf"]) && $_POST['vchCpf'] !=''){
        $alunoEdit->setVchCpf($_POST["vchCpf"]);
    }else{
        header("location :../view/alunos/edit.php");
    }

    if(isset($_POST["vchTelefone"]) && $_POST['vchTelefone'] !=''){
        $alunoEdit->setVchTelefone($_POST["vchTelefone"]);
    }else{
        header("location :../view/alunos/edit.php");
    }

    if(isset($_POST["vchEmail"])){
        $alunoEdit->setVchEmail($_POST["vchEmail"]);
    }else{
        header("location :../view/alunos/edit.php");
    }
        

    if(isset($_POST["tnyEstCivil"]) && $_POST['tnyEstCivil'] !=''){
        
        if($_POST["tnyEstCivil"]=="ST"){
            $alunoEdit->setTnyEstCivil(1);
        }else{
            if($_POST["tnyEstCivil"]=="CS"){
                $alunoEdit->setTnyEstCivil(2);
             }else{
                 if($_POST["tnyEstCivil"] == "DV"){
                     $alunoEdit->setTnyEstCivil(3);
                 }else{
                     $alunoEdit->setTnyEstCivil(4);
                 }
             }
        }
        /*   Valores:
            1 - Solteiro
            2 - Casado
            3 - Divorciado
            4 - Viúvo
         */
    }else{
        header("location :../view/alunos/edit.php");
    }
        
        
        
        // conexão com o banco de dados para atribuir os novos valores
        
        $con = new PDO("mysql:host=127.0.0.1;dbname=escola","root","");
        $stmt = $con->prepare("update alunos set vchNome=?,intIdade=?,bolSexo=?
        ,vchCpf=?,vchTelefone=?,vchEmail=?,tnyEstCivil=? where intIdAluno=?");

        $stmt->bindValue(1,$alunoEdit->getVchNome());
        $stmt->bindValue(2,$alunoEdit->getIntIdade());
        $stmt->bindValue(3,$alunoEdit->getBolSexo());
        $stmt->bindValue(4,$alunoEdit->getVchCpf());
        $stmt->bindValue(5,$alunoEdit->getVchTelefone());
        $stmt->bindValue(6,$alunoEdit->getVchEmail());
        $stmt->bindValue(7,$alunoEdit->getTnyEstCivil());
        $stmt->bindvalue(8,$idAlunoEdit);
        $stmt->execute();
        
        if($stmt->errorCode()>0){
            d($stmt->errorInfo());
            //header("location: .././view./alunos/edit.php?stsMsg=Erro na atualização dos dados&tipoMsg=erro");
        }else{
            header("location: .././view./alunos/view.php?strMsg= Dados atualizados com Sucesso!&tipoMsg=sucesso");
        }
        
    

    
    
    
    
    
    }else{
       header("location: .././view./alunos/edit.php?strMsg=Erro na transmissão da ID do aluno&tipoMsg=erro");
    }


?>
