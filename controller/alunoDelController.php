<?php
     require './kint/Kint.class.php';

    

    if(isset($_GET['idAluno']) && $_GET['idAluno'] != ''){
        
        $IdAlunoApagado = $_GET['idAluno'];
        
        $con = new PDO('mysql:host=127.0.0.1;dbname=escola','root',"");
        $stmt = $con->prepare("delete from alunos where intIdAluno=?");
        $stmt->bindValue(1,$IdAlunoApagado);
        $stmt->execute();
        
        if($stmt->errorCode()>0){
            
            header("location: ../view./alunos/view.php?strMsg=Erro ao Deletar do banco de dados&tipoMsg=erro");
        }else{
            header("location: ../view./alunos/view.php?strMsg=Aluno Deletado com sucesso&tipoMsg=sucesso");
        }
        
    }else{
        header("location :../view/alunos/view.php?strMsg=Erro na passagem de ID do aluno&tipoMsg=erro");
    }

?>