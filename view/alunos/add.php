<?php 

    require '../.././controller./kint/Kint.class.php';
    
    if(isset($_GET['tipoMsg']) && $_GET['tipoMsg'] != ''){
        $tituloMsg = "";
        $mensagem = $_GET['strMsg'];
        $tipoMsg = $_GET['tipoMsg'];
        $classAlert = "";
        
        if($tipoMsg=='erro'){
            $tituloMsg ="Atenção ";
            $classAlert = "alert-danger";
            
        }else{
            if($tipoMsg=='sucesso'){
                $tituloMsg ="Parabéns";
                $classAlert = "alert-success";
            }
        }
    }
    





        $conexao = new PDO("mysql:host=127.0.0.1;dbname=escola","root","",array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        $stmt = $conexao->prepare("select estado.nome,estado.id from estado");
        $stmt->execute();

        if($stmt->errorCode()>0){
            header("location: ../../index.php?strMsg=Erro ao acessar o Banco de dados& tipoMsg=erro");
        }


//<meta charset="utf-8">
?>

<!DOCTYPE html>

<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="../../img/icon.png">
        <title>Formulário</title>
        
        
        <link rel="stylesheet" href="../../css/bootstrap.css">
        <link rel="stylesheet" href="../../css/bootstrap-theme.css">
        <link rel="stylesheet" href="../../css/personalizacao.css">
        
        <script src="../../js/jQuery/jquery-2.1.4.js"></script>
        <script src="../../js/bootstrap.js"></script>
        <script src="../../js/funcoes.js"></script>
         
        
    </head>
    <body>
        <div class="container conteudo-principal">
            <?php if(isset($_GET['tipoMsg']) && $_GET['tipoMsg'] != '' ) { ?> <! -- este conteudo de alert  só será mostrado caso a váriável tipoMsg tenha sido setada e tenha algum valor-->
            
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 ">
                        <div class="alert <?php echo $classAlert; ?> alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong><?php echo $tituloMsg; ?></strong> <?php echo $mensagem; ?>
                        </div>
                    </div>
                </div>
            
            <?php } ?>
            
            <header>
                <div class="row">
                   
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="page-header">
                            <h1>Aluno <small>Cadastro</small></h1>
                        </div>    
                    </div>
                    
                </div>
                
            <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <nav role="navigation" class="navbar navbar-default">
                            <div class="navbar-header">
                                <a class="navbar-brand" href="../../index.php">
                                    <img alt="Brand" src="../../img/icon.png">
                                </a>
                                <button  type="button" data-target="#navBar" data-toggle="collapse" class="navbar-toggle">
                                    <span class="sr-only">Menu alternativo</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div id="navBar" class="collapse navbar-collapse">
                                <ul class="nav navbar-nav navbar-static-top">
                                    <li><a href="../../index.php">Home</a></li>
                                    <!-- dropdown menu alunos -->
                                    <li role="presentation" class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" > Alunos <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li class="active"><a href="add.php">Cadastrar Aluno</a></li>
                                                <li><a href="view.php">Visualizar Matriculados</a></li>
                                                <li class="disabled"><a href="#">Atualizar dados do aluno</a></li>
                                                <li class="disabled"><a href="#">Apagar Aluno</a></li>
                                            </ul>
                                    </li>
                                    <li><a href="../../contato.php ">Fale Conosco</a></li>
                                </ul>
                            </div>
                            
                        </nav>
                    </div>
                </div>
            </header>
            
            <section>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="center-block">
                            <img class="img-responsive img-circle img-icon" src="../.././img/novoAlunoIcon.png">
                            
                            <p class="text-center lead">Ol&aacute;, seja bem vindo a nossa seção de cadastro, preencha os campos corretamente para efetuar sua pre-matrícula.
                            Ap&oacute;s confirmada a pre-matricula, compareça a uma de nossas instituições para confirmar a matricula.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <form class="form-horizontal" action="../../controller/alunoAddController.php" method="post">
                            
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12  col-md-1">
                                    <label class="control-label">*Nome</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <input class="form-control" type="text" required  name="vchNome" />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12 col-md-1">
                                    <label class="control-label">*Idade</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <input class="form-control" type="number" min="1" required name="intIdade"/>
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-12 col-md-1">*Sexo</label>

                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <input  type="radio" name="bolSexo" value="M" required="" />
                                    <label class="control-label">Masculino</label><br/>

                                    <input  type="radio" name="bolSexo" value="F" required="" />
                                    <label class="control-label">Feminino</label><br/>

                                </div>
                                
                            </div>
                            
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12 col-md-1">
                                    <label>*Cpf</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <input class="form-control" type="text" required name="vchCpf" />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12 col-md-1">
                                    <label>*Telefone</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <input class="form-control" type="tel" required name="vchTelefone" />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12 col-md-1">
                                    <label>Email</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <input class="form-control" type="email" name="vchEmail" />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12 col-md-1">
                                    <label>*Estado Civil</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <select class="form-control" name="tnyEstCivil" required>
                                        <option value="">Selecione seu estado civil</option>
                                        <option value="ST">Solteiro</option>
                                        <option value="CS">Casado</option>
                                        <option value="DV">Divorciado</option>
                                        <option value="VV">Viúvo</option>   
                                        <!-- se for colocar outro estado civil atente-se que a proxima ID é 6 e não 5 -->
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12 col-md-1">
                                    <label>*Estado </label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <select id="estados" class="form-control" name="estado" required onchange="getCities()">
                                        <option value="-1">Selecione seu estado</option>
                                        <?php while($linha = $stmt->fetch(PDO:: FETCH_OBJ)){ ?>
                                            <option value="<?php echo $linha->id;?>"><?php echo $linha->nome;?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12 col-md-1">
                                    <label>*Cidade </label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <select id="cidades" class="form-control" name="cidade" required>
                                        <option value="">Selecione sua estado primeiro</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-12 col-md-1"></div>
                                <div class="col-xs-12 col-sm-12 col-md-10">
                                    <button class="btn btn-success" type="submit">Cadastrar</button>
                                    <button class="btn btn-danger" type="reset">Apagar campos</button>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>    
            </section>
            
            <footer>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="well well-sm">
                            <p class="text-center">&copy; Copyright 2015 Jeferson Oliveira <br>
                               &nbsp; &nbsp; &nbsp; &nbsp; <a href="http://www.facebook.com/profile.php?id=100005197385523" target="_blank">Facebook</a> | <a href="https://twitter.com/Jeffoliver97" target="_blank">Twiter</a>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </body>
    
</html>
