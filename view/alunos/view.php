
<?php 

        require '../.././controller./kint/Kint.class.php';

        $conexao = new PDO("mysql:host=127.0.0.1;dbname=escola","root","");
        $stmt = $conexao->prepare("select alunos.intIdAluno,alunos.vchMatricula,alunos.vchNome,alunos.intIdade,alunos.bolSexo, alunos.vchCpf,alunos.vchTelefone,alunos.vchEmail,estado_civil.vchDesc from alunos inner join estado_civil on (alunos.tnyEstCivil = estado_civil.intIdEstado) order by alunos.intIdAluno ");
        $stmt->execute();

        if($stmt->errorCode()>0){
            header("location: ../../index.php?strMsg=Erro ao acessar o Banco de dados& tipoMsg=erro");
        }

?>



<?php 
 
   

    if(isset($_GET['tipoMsg']) && $_GET['tipoMsg'] != ''){
        $tituloMsg = "";
        $mensagem = $_GET['strMsg'];
        $tipoMsg = $_GET['tipoMsg'];
        $classAlert = "";
        
        if($tipoMsg=='erro'){
            $tituloMsg ="Atenção ";
            $classAlert = "alert-danger";
            
        }else{
            if($tipoMsg=='sucesso'){
                $tituloMsg ="Sucesso";
                $classAlert = "alert-success";
            }
        }
    }
    
?>



<!DOCTYPE html>

<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="../../img/icon.png">
        <title>Visualização de Alunos</title>
        
        
        <link rel="stylesheet" href="../../css/bootstrap.css">
        <link rel="stylesheet" href="../../css/bootstrap-theme.css">
        <link rel="stylesheet" href="../../css/personalizacao.css">
        
        <script src="../../js/jQuery/jquery-2.1.4.js"></script>
        <script src="../../js/bootstrap.js"></script>
         
        <script>
            function confirmarDel(event){
                
             var res = confirm("Você realmente deseja deletar este aluno?");
                
                if(res){
                    return true;   
                }else{
                   return event.preventDefault();
                }
            }
        </script>
    </head>
    <body>
        <div class="container conteudo-principal">
            <?php if(isset($_GET['tipoMsg']) && $_GET['tipoMsg'] != '' ) { ?> <! -- este conteudo de alert  só será mostrado caso a váriável tipoMsg tenha sido setada e tenha algum valor-->
            
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 ">
                        <div class="alert <?php echo $classAlert; ?> alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong><?php echo $tituloMsg; ?></strong> <?php echo $mensagem; ?>
                        </div>
                    </div>
                </div>
            
            <?php } ?>
            <header>
                <div class="row">
                   
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="page-header">
                            <h1>Aluno <small>Visualização</small></h1>
                        </div>    
                    </div>
                    
                </div>
                
            <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <nav role="navigation" class="navbar navbar-default">
                            <div class="navbar-header">
                                <a class="navbar-brand" href="../../index.php">
                                    <img alt="Brand" src="../../img/icon.png">
                                </a>
                                <button  type="button" data-target="#navBar" data-toggle="collapse" class="navbar-toggle">
                                    <span class="sr-only">Menu alternativo</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div id="navBar" class="collapse navbar-collapse">
                                <ul class="nav navbar-nav navbar-static-top">
                                    <li><a href="../../index.php">Home</a></li>
                                    <!-- dropdown menu alunos -->
                                    <li role="presentation" class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" > Alunos <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="add.php">Cadastrar Aluno</a></li>
                                                <li class="active"><a href="view.php">Visualizar Matriculados</a></li>
                                                <li class="disabled"><a href="#">Atualizar dados do aluno</a></li>
                                                <li class="disabled"><a href="#">Apagar Aluno</a></li>
                                            </ul>
                                    </li>
                                    <li><a href="../../contato.php ">Fale Conosco</a></li>
                                </ul>
                            </div>
                            
                        </nav>
                    </div>
                </div>
            </header>
            
            <section>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <table class="table table-hover  table-bordered ">
                            <caption>Alunos matriculados</caption>
                            <thead>
                                <tr>
                                    <th>Matrícula</th>
                                    <th>Nome</th>
                                    <th>Idade</th>
                                    <th>Sexo</th>
                                    <th>Cpf</th>
                                    <th>Telefone</th>
                                    <th>Email</th>
                                    <th>Estado Civil</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                        
                            <tbody>
                                <?php while($linha = $stmt->fetch(PDO:: FETCH_OBJ)) {?>
                                        
                                    <tr>  
                                        <td><?php echo $linha->vchMatricula; ?></td>
                                        <td><?php echo $linha->vchNome; ?></td>
                                        <td><?php echo $linha->intIdade; ?></td>
                                        
                                          <td><?php 
                                                if($linha->bolSexo == 0){                                     
                                                    echo "Masculino";
                                                }else{
                                                    echo "Feminino";   
                                                    }
                                              ?></td>  
                                        <td><?php echo $linha->vchCpf; ?></td>
                                        <td><?php echo $linha->vchTelefone; ?></td>
                                        <td><?php echo $linha->vchEmail; ?></td>
                                        <td><?php echo $linha->vchDesc; ?></td>
                                        <td><a class="btn btn-warning" href="edit.php?idAluno=<?php echo $linha->intIdAluno; ?> "><span class="glyphicon glyphicon-edit"></span></a> <a class="btn btn-danger" href="../.././controller/alunoDelController.php?idAluno=<?php echo $linha->intIdAluno; ?>" onclick="confirmarDel(event)"><span class="glyphicon glyphicon-remove"></span></a></td>
                                    </tr>
                                <?php } ?>
                            </tbody>   
                        </table>
                    </div>
                </div>
            </section>
            
            <footer>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="well well-sm">
                            <p class="text-center">&copy; Copyright 2015 Jeferson Oliveira <br>
                               &nbsp; &nbsp; &nbsp; &nbsp; <a href="http://www.facebook.com/profile.php?id=100005197385523" target="_blank">Facebook</a> | <a href="https://twitter.com/Jeffoliver97" target="_blank">Twiter</a>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </body>
    
</html>
