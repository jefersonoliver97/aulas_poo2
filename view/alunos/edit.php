<?php 
 
    
    require '../.././controller./kint/Kint.class.php';
    if(isset($_GET['tipoMsg']) && $_GET['tipoMsg'] != ''){
        $tituloMsg = "";
        $mensagem = $_GET['strMsg'];
        $tipoMsg = $_GET['tipoMsg'];
        $classAlert = "";
        
        if($tipoMsg=='erro'){
            $tituloMsg ="Atenção ";
            $classAlert = "alert-danger";
            
        }else{
            if($tipoMsg=='sucesso'){
                $tituloMsg ="Sucesso";
                $classAlert = "alert-success";
            }
        }
    }
    
?>

<?php
    
    $idALunoEdit = $_GET['idAluno']; // crio uma variável para guardar a ID do aluno que requisitou a edição
    $con = new PDO("mysql:host=127.0.0.1;dbname=escola","root","");
    $stmt = $con->prepare("select alunos.vchNome,alunos.intIdade,alunos.bolSexo,alunos.tnyEstCivil, alunos.vchCpf,alunos.vchTelefone,alunos.vchEmail,estado_civil.vchDesc from alunos inner join estado_civil on (alunos.tnyEstCivil=estado_civil.intIdEstado) where intIdAluno=?");
    $stmt->bindValue(1,$idALunoEdit);
    $stmt->execute();
    
    if($stmt->errorCode()>0){
       header("location: view.php?strMsg=Erro ao acessar banco de dados&tipoMsg=erro");
    }

    
    
    $objAluno = $stmt->fetch(PDO:: FETCH_OBJ); // guardo o objeto retornado pelo select do banco de dados em uma variável
   
   
?>



<!DOCTYPE html>

<html lang="pt-br">
    
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="../../img/icon.png">
        <title>Formulário</title>
        
        
        <link rel="stylesheet" href="../../css/bootstrap.css">
        <link rel="stylesheet" href="../../css/bootstrap-theme.css">
        <link rel="stylesheet" href="../../css/personalizacao.css">
        
        <script src="../../js/jQuery/jquery-2.1.4.js"></script>
        <script src="../../js/bootstrap.js"></script>
         
        
    </head>
    
    <body>
        <div class="container conteudo-principal">
            
            <?php if(isset($_GET['tipoMsg']) && $_GET['tipoMsg'] != '' ) { ?> <! -- este conteudo de alert  só será mostrado caso a váriável tipoMsg tenha sido setada e tenha algum valor-->
            
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 ">
                        <div class="alert <?php echo $classAlert; ?> alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong><?php echo $tituloMsg; ?></strong> <?php echo $mensagem; ?>
                        </div>
                    </div>
                </div>
            
            <?php } ?>
            
            <header>
                <div class="row">
                   
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="page-header">
                            <h1>Aluno <small>Edição de dados</small></h1>
                        </div>    
                    </div>
                    
                </div>
                
            <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <nav role="navigation" class="navbar navbar-default">
                            <div class="navbar-header">
                                <a class="navbar-brand" href="../../index.php">
                                    <img alt="Brand" src="../../img/icon.png">
                                </a>
                                <button  type="button" data-target="#navBar" data-toggle="collapse" class="navbar-toggle">
                                    <span class="sr-only">Menu alternativo</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div id="navBar" class="collapse navbar-collapse">
                                <ul class="nav navbar-nav navbar-static-top">
                                    <li><a href="../../index.php">Home</a></li>
                                    <!-- dropdown menu alunos -->
                                    <li role="presentation" class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" > Alunos <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li class="active"><a href="add.php">Cadastrar Aluno</a></li>
                                                <li><a href="view.php">Visualizar Matriculados</a></li>
                                                <li class="disabled"><a href="#">Atualizar dados do aluno</a></li>
                                                <li class="disabled"><a href="#">Apagar Aluno</a></li>
                                            </ul>
                                    </li>
                                    <li><a href="../../contato.php ">Fale Conosco</a></li>
                                </ul>
                            </div>
                            
                        </nav>
                    </div>
                </div>
            </header>
            
            <section>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="center-block">
                            <img class="img-responsive img-circle img-icon" src="../.././img/novoAlunoIcon.png">
                            
                            <p class="text-center lead">Por favor preencha todos os campos corretamente
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <form class="form-horizontal" action="../../controller/alunoEditController.php" method="post">
                            
                            <input type="hidden" name="idAluno" value="<?php echo $idALunoEdit ; ?>" /> <!-- imput para guardar a Id deste aluno para passar pra poxima tela -->
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12  col-md-1">
                                    <label class="control-label">*Nome</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <input class="form-control" type="text"  name="vchNome" value="<?php echo $objAluno->vchNome; ?>" />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12 col-md-1">
                                    <label class="control-label">*Idade</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <input class="form-control" type="number" min="1" required name="intIdade" value="<?php echo $objAluno->intIdade; ?>">
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-12 col-md-1">*Sexo</label>

                                
                                
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                
                                    <?php 
                                        if($objAluno->bolSexo == 0){ ?>   
                                            <input  type="radio" name="bolSexo" value="M" required="" checked />
                                            <label class="control-label">Masculino</label><br/>

                                            <input  type="radio" name="bolSexo" value="F" required="" />
                                            <label class="control-label">Feminino</label><br/>
                                    
                                        <?php }else{?>
                                    
                                            <input  type="radio" name="bolSexo" value="M" required="" />
                                            <label class="control-label">Masculino</label><br/>

                                            <input  type="radio" name="bolSexo" value="F" required="" checked />
                                            <label class="control-label">Feminino</label><br/>
                                        <?php }?>
                                

                                </div>
                                
                            </div>
                            
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12 col-md-1">
                                    <label>*Cpf</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <input class="form-control" type="text" required name="vchCpf" value="<?php echo $objAluno->vchCpf; ?>"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12 col-md-1">
                                    <label>*Telefone</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <input class="form-control" type="tel" required name="vchTelefone"  value="<?php echo $objAluno->vchTelefone; ?>"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12 col-md-1">
                                    <label>Email</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <input class="form-control" type="email" name="vchEmail" value="<?php echo $objAluno->vchEmail ; ?>"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="control-label col-xs-12 col-sm-12 col-md-1">
                                    <label>*Estado Civil</label>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-11">
                                    <select class="form-control" name="tnyEstCivil" required>
                                        <?php 
                                            $opsSelect = "";

                                            switch($objAluno->tnyEstCivil){
                                                case '1':{
                                                            $opsSelect = " 
                                                    <option value=''>Selecione seu estado civil</option>
                                                    <option value='ST' selected>Solteiro</option>
                                                    <option value='CS'>Casado</option>
                                                    <option value='DV'>Divorciado</option>
                                                    <option value='VV'>Viúvo</option>  ";
                                                    break;
                                                }
                                                case'2':{
                                                                $opsSelect = " 
                                                    <option value=''>Selecione seu estado civil</option>
                                                    <option value='ST'>Solteiro</option>
                                                    <option value='CS' selected>Casado</option>
                                                    <option value='DV'>Divorciado</option>
                                                    <option value='VV'>Viúvo</option>  ";
                                                    break;
                                                }
                                                case '3':{
                                                    
                                                                $opsSelect = " 
                                                    <option value=''>Selecione seu estado civil</option>
                                                    <option value='ST'>Solteiro</option>
                                                    <option value='CS'>Casado</option>
                                                    <option value='DV' selected>Divorciado</option>
                                                    <option value='VV'>Viúvo</option>  ";
                                                    break;
                                                }
                                                case '4':{
                                                            $opsSelect = " 
                                                    <option value=''>Selecione seu estado civil</option>
                                                    <option value='ST'>Solteiro</option>
                                                    <option value='CS'>Casado</option>
                                                    <option value='DV'>Divorciado</option>
                                                    <option value='VV' selected>Viúvo</option>  ";
                                                    break;
                                                        }
                                                default: {
                                                            $opsSelect = " 
                                                    <option value=''>Selecione seu estado civil</option>
                                                    <option value='ST'>Solteiro</option>
                                                    <option value='CS'>Casado</option>
                                                    <option value='DV'>Divorciado</option>
                                                    <option value='VV'>Viúvo</option>  ";
                                                }
                                                    
                                            }
                                           
                                        ?>
                                        
                                        <?php echo $opsSelect; ?>
                                        
                                        <!-- se for colocar outro estado civil atente-se que a proxima ID é 6 e não 5 pois foi inserida e deletada um estado civil -->
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-12 col-md-1"></div>
                                <div class="col-xs-12 col-sm-12 col-md-10">
                                    <button class="btn btn-success " type="submit">Salvar novos dados</button>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>    
            </section>
            
            <footer>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="well well-sm">
                            <p class="text-center">&copy; Copyright 2015 Jeferson Oliveira <br>
                               &nbsp; &nbsp; &nbsp; &nbsp; <a href="http://www.facebook.com/profile.php?id=100005197385523" target="_blank">Facebook</a> | <a href="https://twitter.com/Jeffoliver97" target="_blank">Twiter</a>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </body>
    
</html>
