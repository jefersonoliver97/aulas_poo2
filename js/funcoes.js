
function getCities(){
    
    if($("#estados").val() != "-1"){
        $("#cidades").empty();
        
        $.ajax({
        url:"../../controller/pegaCidades.php",
        data:{id:$("#estados").val()},
        success: function (pData,pStaus,pJQxhr){
        $("#cidades").append("<option value='-1'>Selecione sua cidade</option>");
            for(i=0;i< pData.length;i++){
                var opt = $("<option/>");
                opt.attr("value",pData[i].id);
                opt.text(pData[i].nome);
                $("#cidades").append(opt);
            }
        },
        dataType: "json"
        });
    }
    
}

